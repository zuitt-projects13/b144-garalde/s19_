console.log('JavaScript ES6 Update Activity')

let num = parseInt(prompt("Enter a number: "))
let getCube = num ** 3

console.log(`The cube of ${num} is ${getCube}.`)


let address = [258, 'Washington Ave NW', 'California', 90011] 

let [houseNo, street, state, zipCode] = address

console.log(`I live at ${houseNo} ${street}, ${state} ${zipCode}`)


let animal = {
	name: 'Lolong',
	familyClass: 'saltwater crocodile',
	weight: 1075,
	measurement: '20 ft 3 in'
}

let {name,familyClass,weight,measurement} = animal

console.log(`${name} was a ${familyClass}. He weighed at ${weight} kgs with a measurement of ${measurement}.` )


let numArray = [1,2,3,4,5]

numArray.forEach((nums) => console.log(nums))
// nums = the elements of the numArray
let reduceNumber = numArray.reduce((total,nums) => total + nums ,0)


console.log(reduceNumber)


class Dog {
	constructor (name,age,breed){
	this.name = name,
	this.age = age,
	this.breed = breed
	}
}


let theDog = new Dog('Frankie',5,'Miniature Dachshund')

console.log(theDog)