// JavaScript ES6 Updates

//1. EXPONENT OPERATOR

// pre version
const firstNum = Math.pow(8,2);
console.log(firstNum)


// ES6
const secondNum = 8 ** 2
console.log(secondNum)


console.log('Hello')


// 2. TEMPLATE LITERALS
// allows us to write string without using the concatenation operator (+)

// pre-template literal string
// uses single quotes ''
let name = 'John'
let message = 'Hello ' + name + '! Welcome to programming!'
console.log('Message without template literals: ' + message)

// ES6
// strings using template literal using backticks ``

message = `Hello ${name}! Welcome to programming!`
console.log(`Message with template literals: ${message}`)

// multi-line using template literals
const anotherMessage = `${name} attended a math competition. He won it by solving the problem 8 ** 2 with solution of ${secondNum}`

console.log(anotherMessage)

// template literals allow us to write strings with embedded JS expressions ${}

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal*interestRate}`)


// 3. ARRAY DESTRUCTURIN

// allows us to unpack elements in arrays into distinct variables.

// allows us to name array elements with variables instead of using index numbers. Helps with code readability.

// syntax: let const[variableName,variableName] = array

const fullName = ['Juan', 'Dela', 'Cruz']

// pre array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);


console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]} It's nice to meet you`)

// ES6 array destructuring
const [firstName, middleName, lastName]= fullName

console.log(firstName)
console.log(middleName)
console.log(lastName)

// expressions are any valid unit of code that resolves to a value
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`)

// 4. OBJECT DESTRUCTURING
// allows us to unpack elements in objects into distinct variables

const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
}

// pre object destructuring
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`)

// ES6 object destructuring
// shortens the syntax for accessing properties from objects
// syntax : let/const {propertyName,propertyName} = object

const { givenName, maidenName, familyName} = person

console.log(givenName)
console.log(maidenName)
console.log(familyName)

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`)


function getFullName({givenName, maidenName, familyName}) {
	console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person)

// 5. ARROW Functions

// normal function
// parameters = placeholder/ the same of an argument to be passed to the function
// statement = function body
// invoke/call back function

function printFullName(fName,mName,lName) {
 	console.log(fName + ' ' + mName + ' ' + lName)
}


printFullName('John', 'D.','Smith')

// ES6 ARROW FUNCTION
// compact alternative syntax to traditional functions
// useful for code snippets where creating functions will not be reused in any other portion of the code
// adheres to the  'DRY' principle (Don't Repeat Yourself) where there's no longer need to create a function that will only be used in certain snippets
const variableName = () => {
	console.log('Hello World')
}



const printFullNameA = (fName,mName,lName) => {
	console.log(fName + ' ' + mName + ' ' + lName)
}

printFullNameA('Jane', 'D','Smith')


// ARROW Function with loops
// Pre arrow function

const students = ['John','Jane','Joe']

students.forEach(function(student)
	{
		console.log(`${student} is a student`)
	}

	)


// arrow function
students.forEach((student) => console.log(`${student} is a student.`));

// 6. ARROW FUNCTION - Implicit Return Statement
// there are instances where you can omit the 'return' statement. This works because even without the 'return' statement, javascript implicitly adds it to the result of the function


// pre arrow function

/*const add = (x,y) => {
	return x + y;
}

let total = add(1,2);
console.log(total)*/

// arrow function
const add = (x,y) => x + y

let total = add(1,2);
console.log(total)

/*let filterFriends = friends.filter(friends => friend.length === )

we can ommit the parenthesis () if there is only one parameter

*/

// 7. ARROW FUNCTION Default function argument value
// Provides a defualt argument value if none is provided when the function is invoked


// where 'User'is the default value if no input was provided
const greet = (name = 'User') => {
	return `Good morning, ${name}`
}
console.log('with input')
console.log(greet('Jane'))
console.log('using default')
console.log(greet())


// 8. CREATING A CLASS
// uses constructor
/*
syntax:
	class className {
		constructor(objectPropertyA,objectPropertyB) {
			this.objectPropertyA = objectPropertyA
			this.objectPropertyB = objectPropertyB

		}
	}

the constructor is a special method of a class for creating / initializing an object for that class.

'this' refers to the properties of an object created from the class.

*/

class car {
	constructor(brand, name, year){
		this.brand = brand;
		this.year = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar)

myCar.brand = 'Ford'
myCar.name = 'Ranger Raptor'
myCar.year = 2021;

// creating /instantiating a ne object from the car class with initialized values

const myNewCar = new Car('Toyota','Vios',2021)


console.log(myNewCar)

// 9. TERNARY OPERATOR
// conditional operator
// it has 3 operands (condition <question mark(?) will serve as IF>, expression to execute if the condition is true followed by colon : and finally the expression to execute if the condition is false)

// pre ternary
if (condition == 0) {
	return true
}
else {
	return false
}

// ternary
// to use the ternary operator it must be inside an object
(condition == 0) ? true : false





